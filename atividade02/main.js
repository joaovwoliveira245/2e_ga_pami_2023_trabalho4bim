const createHTML = () => {
  const root = document.getElementById("root");

  const template = `
  <section>
    <div>
      <label for="name">Nome</label>
      <input type="text" placeholder="Digite aqui" id="name">
    </div>

    <div>
      <label for="year">Ano que você nasceu</label>
      <input type="number" placeholder="Digite aqui" id="year">
    </div>

    <div>
      <div class="buttons">
        <button>Verificar</button>
        <button id="clear">Limpar</button>
      </div>
    </div>

    <span></span>
  </section>
  `;

  root.innerHTML = template;
}

const getValues = () => {
  const name = document.querySelector("input#name").value;
  const year = document.querySelector("input#year").value;

  return {
      name: String(name),
      year: Number(year)
  }
}

const handleEvents = () => {
  const button = document.querySelector("button");
  const clear = document.querySelector("button#clear");
  const answer = document.querySelector("span");

  button.addEventListener('click', () => {
    const { name, year } = getValues();
    const currentYear = new Date().getFullYear();
    const age = currentYear - year;
    let message = `Seu nome é ${name}.<br>Sua idade é ${age}.<br>`;

    if (age >= 18) {
      message += `Você já é maior de idade!`;
    } else {
      message += `Você ainda não é maior de idade.`;
    }
    answer.innerHTML = message;
  })

  clear.addEventListener('click', () => {
    answer.innerHTML = '';
  })
}

createHTML();
handleEvents();