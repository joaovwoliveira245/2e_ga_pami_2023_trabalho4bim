const createHTML = () => {
  const root = document.getElementById("app");

  const template = `
  <section>
    <div>
      <label>Nome</label>
      <input type="text" placeholder="Insira seu nome" id="name">
    </div>
    <div>
      <label>Sexo</label>
      <select>
        <option value="Masculino" checked>Masculino</option>
        <option value="Feminino">Feminino</option>
      </select>
    </div>
    <div>
      <div class="buttons">
        <button id="button">Verificar</button>
        <button id="clear">Limpar</button>
      </div>
      <span></span>
    </div>
  </section>
  `

  root.innerHTML = template;
}

const getValues = () => {
  const name = document.querySelector("#name").value;
  const gender = document.querySelector("select").value;

  return {
    name: String(name),
    gender: String(gender)
  }
}

const handleEvents = () => {
  const button = document.querySelector("#button");
  const clear = document.querySelector("#clear");
  const span = document.querySelector("span");

  button.addEventListener('click', () => {
    const {name, gender} = getValues();

    if (gender == `Masculino` || gender == `Feminino`) {
      span.innerHTML = `Seu nome é ${name}, sexo informado ${gender}`;
    } else {
      span.innerHTML = `Seu nome é ${name}, sexo não reconhecido`;
    }
  })

  clear.addEventListener('click', () => {
    span.innerHTML = ``;
  })
}

createHTML();
handleEvents();