const createHTML = () => {
  const root = document.querySelector("#root");

  const template = `
  <section>
    <div>
      <label for="wage">Salário mensal</label>
      <input type="number" placeholder="Digite aqui" id="wage">
    </div>
    <div>
      <label for="product">Produto que deseja comprar</label>
      <input type="text" placeholder="Digite aqui" id="product">
    </div>
    <div>
      <label for="price">Preço do produto</label>
      <input type="number" placeholder="Digite aqui" id="price">
    </div>
    <div>
      <div class="buttons">
        <button>Verificar</button>
        <button id="clear">Limpar</button>
      </div>
    </div>
    <div>
      <span></span>
    </div>
  </section>
  `

  // 3) Faça uma aplicação onde o usuário irá digitar o salário que recebe mensalmente.
  // Depois solicite que informe algo que queira adquirir, por exemplo, um notebook. Em
  // seguida ele deve digitar o preço do item desejado. Verifique e mostre para o usuário
  // se ele pode ou não comprar o que deseja e se vai sobrar ou faltar dinheiro para
  // isso.

  root.innerHTML = template;
}

const getValues = () => {
  const wage = document.querySelector("#wage").value;
  const product = document.querySelector("#product").value;
  const price = document.querySelector("#price").value;

  return {
    wage: Number(wage),
    product: String(product),
    price: Number(price)
  }
}

const handleEvents = () => {
  const button = document.querySelector("button");
  const clear = document.querySelector("button#clear");
  const answer = document.querySelector("span");

  button.addEventListener('click', () => {
    const { wage, product, price } = getValues();
    let message = `Seu salário é: R$ ${wage}<br>O produto que você quer comprar é ${product} (R$ ${price})<br>`

    if (wage > 0 && product != `` && price > 0) {
      if (wage == price) {
        message += `Você vai poder comprar o produto!<br>Mas não terá troco`
      } else if (wage > price) {
        message += `Você vai poder comprar o produto!<br>E vai receber R$${wage - price} de troco!`
      } else {
        message += `Você não pode comprar o produto.<br>Ainda faltam R$${(wage - price) * -1}.`
      }
    } else {
      message = `Preencha todos os campos.`
    }

    answer.innerHTML = message;
  })

  clear.addEventListener('click', () => {
    answer.innerHTML = '';
  })
}

createHTML();
handleEvents();