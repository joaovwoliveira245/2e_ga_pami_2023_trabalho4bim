const createHTML = () => {
  const root = document.getElementById("root");

  const template = `
  <section>
    <div>
      <h1>Menu</h1>
      <span id="hr"></span>
    </div>
    <div>
      <h2>Entradas</h2>
      <ul id="entradas">
        <li>
          <label for="Fritas com cheddar e bacon">Fritas com cheddar e bacon<br>R$ 23.90</label>
          <input type="checkbox" id="Fritas com cheddar e bacon" data-price="23.90">
        </li>
        <li>
          <label for="Tiras de Frango">Tiras de Frango<br>R$ 32.90</label>
          <input type="checkbox" id="Tiras de Frango" data-price="32.90">
        </li>
        <li>
          <label for="Calabresa com cebola">Calabresa com cebola<br>R$ 19.99</label>
          <input type="checkbox" id="Calabresa com cebola" data-price="19.99">
        </li>
      </ul>
    </div>
    <div>
      <h2>Principais</h2>
      <ul id="principais">
        <li>
          <label for="Macarronada">Macarronada<br>R$ 24</label>
          <input type="checkbox" id="Macarronada" data-price="24">
        </li>
        <li>
          <label for="Churrasco">Churrasco<br>R$ 49.90</label>
          <input type="checkbox" id="Churrasco" data-price="49.90">
        </li>
        <li>
          <label for="Salmão grelhado">Salmão grelhado<br>R$ 65.50</label>
          <input type="checkbox" id="Salmão grelhado" data-price="65.50">
        </li>
      </ul>
    </div>
    <div>
      <h2>Bebidas</h2>
      <ul id="bebidas">
        <li>
          <label for="Suco de Laranja">Suco de Laranja<br>R$ 4.99</label>
          <input type="checkbox" id="Suco de Laranja" data-price="4.99">
        </li>
        <li>
          <label for="Refrigerante">Refrigerante<br>R$ 3.50</label>
          <input type="checkbox" id="Refrigerante" data-price="3.50">
        </li>
        <li>
          <label for="Água com gás">Água com gás<br>R$ 2</label>
          <input type="checkbox" id="Água com gás" data-price="2">
        </li>
      </ul>
    </div>
    <div>
      <h2>Sobremesas</h2>
      <ul id="sobremesas">
        <li>
          <label for="Sorvete">Sorvete<br>R$ 9.90</label>
          <input type="checkbox" id="Sorvete" data-price="9.90">
        </li>
        <li>
          <label for="Açaí com Sorvete">Açaí com Sorvete<br>R$ 12.90</label>
          <input type="checkbox" id="Açaí com Sorvete" data-price="12.90">
        </li>
        <li>
          <label for="Bolo de chocolate">Bolo de chocolate<br>R$ 12.90</label>
          <input type="checkbox" id="Bolo de chocolate" data-price="12.90">
        </li>
      </ul>
    </div>
    <div>
      <div class="buttons">
        <button>Ver pedido</button>
      </div>
    </div>
  </section>
  `;

  root.innerHTML = template;
}

let total = 0;
const items = [];

const handleEvents = () => {
  const checkboxes = document.querySelectorAll('input[type="checkbox"]');
  checkboxes.forEach(checkbox => {
    checkbox.addEventListener('change', (event) => {
      const index = items.findIndex(item => item.name === event.target.id);
      if (event.target.checked) {
        const quantity = prompt('Quantos você deseja?');
        const price = event.target.getAttribute('data-price');
        const totalItem = quantity * price;
        total += totalItem;
        if (index !== -1) {
          items[index] = {name: event.target.id, quantity, total: totalItem};
        } else {
          items.push({name: event.target.id, quantity, total: totalItem});
        }
      } else if (index !== -1) {
        total -= items[index].total;
        items.splice(index, 1);
      }
    });
  });

  const button = document.querySelector('button');
  button.addEventListener('click', () => {
    let message = '';
    items.forEach(item => {
      message += `${item.quantity} ${item.name} | Total: R$ ${item.total.toFixed(2)}\n`;
    });
    if (total == 0) {
      message = `Nenhum produto adicionado!`;
    } else {
      message += `\nTotal geral: R$ ${total.toFixed(2)}`;
    }
    window.alert(message);
  });
}

createHTML();
handleEvents();
